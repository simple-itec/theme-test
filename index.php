<?php wp_head(); ?>

<div class="wrapper">
	<div class="box-title">
		<h1>Test Sass</h1>
	</div>
	<div class="box-menu">
		<div class="articles">
			<ul>
				<li><a href="#" class="menu-item home"></a></li>
				<li><a href="#" class="menu-item">Hardware</a></li>
				<li><a href="#" class="menu-item">Training</a></li>
				<li><a href="#" class="menu-item">WebDev</a></li>
				<li><a href="#" class="menu-item">DevOps</a></li>
				<li><a href="#" class="menu-item">Cloud</a></li>
				<li><a href="#" class="menu-item">Network</a></li>
				<li><a href="#" class="menu-item">Lab</a></li>
				<li><a href="#" class="menu-item">Management</a></li>
			</ul>
		</div>
		<div class="pages">
			<ul>
				<li><a href="#" class="menu-item">Projects</a></li>
			</ul>
		</div>
	</div>
	<div class="box-articles">
		<div class="item-article">
			<h6>Tag: test</h6>
			<h2>Titre d'article</h2>
			<h3>Sous-titre</h3>
			<h4>Description</h4>
			<h5>Publié par: test</h5>
			<p>Ceci est le corps du texte.</p>
		</div>
		<div class="item-article">
			<?php if (have_posts()) : the_post(); ?>
				<?php 
					the_title('<h2>', '</h2>');
					the_excerpt('<p>', '</p>');
					the_date('j F Y','<h6>', '</h6>','$true');
				?>
			<?php endif; ?>
		</div>
		<div class="item-article">
			Article 3
		</div>
		<div class="item-article">
			Article 4
		</div>
		<div class="item-article">
			Article 5
		</div>
		<div class="item-article">
			Article 6
		</div>
	</div>
	<div class="box-pages">
		<h6>Tag: test</h6>
		<h2>Titre d'une page</h2>
		<h3>Sous-titre</h3>
		<h4>Description</h4>
		<h5>Publié par: test</h5>
		<p>Ceci est le corps du texte.</p>		
	</div>
</div>


<?php wp_footer(); ?>
