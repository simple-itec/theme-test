<?php

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles');

function my_theme_enqueue_styles() {
 
    // Add based CSS of the theme
    wp_enqueue_style('style.css', get_template_directory_uri() . '/style.css');

    // Add customizing CSS into the theme
    wp_enqueue_style('test.css', get_template_directory_uri() . '/css/test.css');
}


?>